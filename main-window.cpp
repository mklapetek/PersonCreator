/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Martin Klapetek <email>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QtGui/QFormLayout>
#include <QSortFilterProxyModel>

#include <Nepomuk2/Resource>
#include <Nepomuk2/Vocabulary/NCO>
#include <Nepomuk2/Vocabulary/PIMO>
#include <Nepomuk2/Variant>

#include <Soprano/Vocabulary/NAO>

#include <KCategoryDrawer>
#include <KDebug>

#include "main-window.h"
#include "persons-model.h"
#include "contacts-model.h"
#include "filter-bar.h"
#include "person-object.h"
#include "name-select-dialog.h"
#include "persons-proxy-model.h"

MainWindow::MainWindow(QWidget* parent)
    : KMainWindow(parent),
    m_personsModel(0),
    m_personsProxyModel(0),
    m_contactsModel(0),
    m_contactsProxyModel(0)
{
    setupUi(this);

    m_personsModel = new PersonsModel();
    m_personsProxyModel = new PersonsProxyModel();
    m_personsProxyModel->setSourceModel(m_personsModel);
    m_personsProxyModel->setDynamicSortFilter(true);
    m_personsProxyModel->setCategorizedModel(true);

    m_contactsModel = new ContactsModel();
    m_contactsProxyModel = new QSortFilterProxyModel(this);
    m_contactsProxyModel->setSourceModel(m_contactsModel);
    m_contactsProxyModel->sort(0);

    m_listView->setModel(m_contactsProxyModel);
    m_personsView->setModel(m_personsProxyModel);
    m_personsView->setCategoryDrawer(new KCategoryDrawerV3(m_personsView));

    m_personsProxyModel->sort(0);

    m_toolBar->setToolButtonStyle(Qt::ToolButtonFollowStyle);

    connect(m_filterBar, SIGNAL(filterChanged(QString)),
            m_contactsProxyModel, SLOT(setFilterFixedString(QString)));

    KAction *m_mergeAction = new KAction(KIcon("user-group-new"), i18nc("Toolbar action that merges selected contacts into one person", "Merge"), m_toolBar);
    connect(m_mergeAction, SIGNAL(triggered(bool)),
            this, SLOT(prepareMerging()));

    m_toolBar->addAction(m_mergeAction);

    KAction *m_addToSelectedAction = new KAction(KIcon("list-add-user"), i18nc("Toolbar action that adds selected contacts to already created (and selected) person",
                                                                               "Add to selected person"), m_toolBar);
    connect(m_addToSelectedAction, SIGNAL(triggered(bool)),
            this, SLOT(addContactToSelectedPerson()));

    m_toolBar->addAction(m_addToSelectedAction);

    KAction *m_savePersons = new KAction(KIcon("document-save"), i18nc("Toolbar action that saves created persons into Nepomuk", "Save persons"), m_toolBar);

    connect(m_savePersons, SIGNAL(triggered(bool)),
            m_personsModel, SLOT(savePersons()));

    m_toolBar->addAction(m_savePersons);

    m_personDetailsView->setColumnCount(3);
    m_personDetailsView->setHorizontalHeaderLabels(QStringList() << i18n("Contact name") << i18n("Email") << i18n("Phone number"));

    connect(m_personsView, SIGNAL(clicked(QModelIndex)),
            this, SLOT(showContactDetails(QModelIndex)));

}

MainWindow::~MainWindow()
{

}

void MainWindow::prepareMerging()
{
    addContactsToPerson(m_listView->selectionModel()->selectedIndexes(), new PersonObject());

    m_listView->clearSelection();
}

void MainWindow::addContactsToPerson(const QModelIndexList& contactIndexes, PersonObject* person)
{
    Q_ASSERT(person);
    QHash<QString, int> labels;
    foreach (QModelIndex index, contactIndexes) {
        QString label = index.data(Qt::DisplayRole).toString();
        labels.insert(label, labels.values(label).size()+1);
        person->addContact(index.data(ContactsModel::UriRole).toUrl());
    }

    if (person->label().isEmpty()) {
        QHashIterator<QString, int> it(labels);
        bool singleMaxExists = false;
        int max = 0;
        QString maxKey;

        while (it.hasNext()) {
            it.next();
            if (it.value() > max) {
                max = it.value();
                singleMaxExists = true;
                maxKey = it.key();
            } else if (it.value() == max) {
                singleMaxExists = false;
            }
        }

        if (singleMaxExists) {
            person->setLabel(maxKey);
        } else {
            NameSelectDialog *dialog = new NameSelectDialog(labels.keys(), this);
            dialog->exec();
            int response = dialog->clickedButton();
            if (response > 10000) {
                person->setLabel(QString(labels.keys().at(response - 10001)));
            } else {
                return;
            }
        }
    }

    m_personsModel->insertPerson(person);
    m_personsProxyModel->sort(0);
}


void MainWindow::addContactToSelectedPerson()
{
    PersonObject *person = m_personsView->selectionModel()->selectedIndexes().first().data(Qt::UserRole).value<PersonObject*>();

    Q_ASSERT(person);
    addContactsToPerson(m_listView->selectionModel()->selectedIndexes(), person);
    showContactDetails(m_personsView->currentIndex());

    m_listView->clearSelection();
}

void MainWindow::showContactDetails(QModelIndex index)
{
    m_personDetailsView->clearContents();
    m_personDetailsView->setRowCount(0);

    PersonObject *person = m_personsView->selectionModel()->selectedIndexes().first().data(Qt::UserRole).value<PersonObject*>();

    Q_ASSERT(person);

    kDebug() << person->allContacts();

    int i = 0;
    foreach (QUrl uri, person->allContacts()) {
        Nepomuk2::Resource r(uri);
        QTableWidgetItem *nameItem = new QTableWidgetItem();
        QTableWidgetItem *emailItem = new QTableWidgetItem();
        QTableWidgetItem *phoneItem = new QTableWidgetItem();

        if (r.hasProperty(Soprano::Vocabulary::NAO::prefLabel())) {
            nameItem->setText(r.property(Soprano::Vocabulary::NAO::prefLabel()).toString());
        }

        if (r.hasProperty(Nepomuk2::Vocabulary::NCO::hasEmailAddress())) {
            Nepomuk2::Resource email(r.property(Nepomuk2::Vocabulary::NCO::hasEmailAddress()).toResource());
            QString emailString;
            foreach (QString e, email.property(Nepomuk2::Vocabulary::NCO::emailAddress()).toStringList()) {
                emailString.append("%1, ").arg(e);
            }

            emailItem->setText(emailString.left(emailString.length() - 2)); //remove the last comma with space
        }

        if (r.hasProperty(Nepomuk2::Vocabulary::NCO::hasPhoneNumber())) {
            Nepomuk2::Resource phone(r.property(Nepomuk2::Vocabulary::NCO::hasPhoneNumber()).toResource());
            QString phoneString;
            foreach (QString p, phone.property(Nepomuk2::Vocabulary::NCO::phoneNumber()).toStringList()) {
                phoneString.append("%1, ").arg(p);
            }

            phoneItem->setText(phoneString.left(phoneString.length() - 2));
        }

        m_personDetailsView->setRowCount(m_personDetailsView->rowCount() + 1);
        m_personDetailsView->setItem(i, 0, nameItem);
        m_personDetailsView->setItem(i, 1, emailItem);
        m_personDetailsView->setItem(i++, 2, phoneItem);
    }
}
