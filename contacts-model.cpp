/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Martin Klapetek <email>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <Nepomuk2/Resource>
#include <Nepomuk2/ResourceManager>
#include <Nepomuk2/Vocabulary/NCO>
#include <Nepomuk2/Vocabulary/PIMO>
#include <Nepomuk2/Variant>
// #include <Nepomuk2/Thing>

#include <nepomuk2/simpleresource.h>
#include <nepomuk2/simpleresourcegraph.h>

#include <Soprano/Vocabulary/NAO>
#include <Soprano/QueryResultIterator>
#include <Soprano/Node>
#include <Soprano/Model>

#include <KDebug>

#include "contacts-model.h"

ContactsModel::ContactsModel()
    : m_resList(0)
{
    m_resList = new QList<Nepomuk2::Resource>();

    // Nepomuk2::ResourceManager::instance()->clearCache();

    QString query;
    query = "select ?r where { ?r a nco:Contact . }";

    Soprano::QueryResultIterator it = Nepomuk2::ResourceManager::instance()->mainModel()->executeQuery(query,
                                                                                                      Soprano::Query::QueryLanguageSparql);

    while(it.next()) {
//         kDebug() << Nepomuk2::Resource(it["r"].uri()).allProperties();
        m_resList->append(Nepomuk2::Resource(it["r"].uri()));
    }
}

ContactsModel::~ContactsModel()
{

}

QVariant ContactsModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    switch(role) {
        case Qt::DisplayRole:
            return m_resList->at(index.row()).property(Soprano::Vocabulary::NAO::prefLabel()).variant();
            //         case PersonsModel::ItemRole:
            //return QVariant::fromValue<Nepomuk2::Resource>(m_resList->at(index.row()));
//         case Qt::DecorationRole:
//             return SmallIcon("user-identity", KIconLoader::SizeMedium);
        case ContactsModel::UriRole:
            return m_resList->at(index.row()).uri();
        default:
            return QVariant();
    }
}

int ContactsModel::rowCount(const QModelIndex& parent) const
{
    return m_resList->size();
}
