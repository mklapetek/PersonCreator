/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Martin Klapetek <email>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef PERSON_OBJECT_H
#define PERSON_OBJECT_H

#include <QObject>
#include <QMetaType>
#include <QUrl>

class PersonObject : public QObject
{
    Q_OBJECT
public:
    PersonObject(const QString& label = QString(), QObject* parent = 0);
    PersonObject(const PersonObject& other);
    ~PersonObject();

    QString label();
    void setLabel(const QString& label);

    QUrl uri();
    void setUri(const QUrl& uri);

    QList<QUrl> allContacts();
    void addContact(const QUrl& url);
    void removeContact(const QUrl& url);
    void addContacts(const QList<QUrl> contacts);

private:
    QString m_label;
    QUrl m_uri;
    QList<QUrl> m_contacts;
};

typedef PersonObject * PersonObjectPtr;
Q_DECLARE_METATYPE(PersonObjectPtr)

#endif // PERSON_OBJECT_H
