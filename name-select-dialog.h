/*
 *  A dialog which provides at least three choices, plus a cancel button
 *  Based on digiKam's TripleChoiceDialog class
 *
 *  Copyright (C) 2010-2011 by Marcel Wiesweg <marcel dot wiesweg at gmx dot de>
 *  Copyright (C) 2011  Martin Klapetek <martin dot klapetek at gmail dot com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef NAMESELECTDIALOG_H
#define NAMESELECTDIALOG_H

#include <KDialog>

class QToolButton;

class NameSelectDialog : public KDialog
{
    Q_OBJECT

public:

    /**
     * This dialog provides a widget containing a number of buttons.
     * For two options and less, traditional Ok | Apply | Cancel is sufficient,
     * but with one more option Ok | Apply | User1 and Cancel, there is one
     * button too much for the traditional layout.
     *
     * Note that per default, the buttonContainer() widget is not added anywhere.
     * Instead, add it to the layout when you setup the dialog's main widget.
     */

    NameSelectDialog(const QList<QString>& list, QWidget* parent = 0);
    ~NameSelectDialog();

    /**
     * Sets the icon size of the added buttons.
     * Default: KIconLoader::SizeMedium.
     * Note: Call before addButton() or buttonContainer().
     */
    void setIconSize(int iconSize);
    int iconSize() const;

    /**
     * Change the visibility of the Cancel button. Default: True.
     * The Cancel button is at the normal position in the dialog.
     */
    void setShowCancelButton(bool show);

    /**
     * Add a button to the dialog.
     * This button is not positioned like a normal dialog button
     * at the bottom, but in a central area.
     * Each added button has a unique, associated keycode (see KDialog)
     * Note: The Ok button is automatically bound to the Return key press.
     */
    QToolButton* addChoiceButton(int key, const QString& iconName, const QString& text);
    QToolButton* addChoiceButton(int key, const QString& text);
    QToolButton* choiceButton(int key) const;

    /**
     * Returns the button that was clicked
     */
    int clickedButton() const;

protected:

    /**
     * Handles button presses.
     * The default implementation, emits buttonClicked(),
     * rejects in case of cancel, otherwise accepts.
     * Note: Normal KDialog events, for the Cancel button or pressing
     * the Esc or Return keys, arrive here too.
     */
    virtual void slotButtonClicked(int button);

    /**
     * Returns the widget that contains the buttons.
     * You must place this widget in your dialogs layout.
     */
    QWidget* buttonContainer() const;

private:

    class NameSelectDialogPriv;
    NameSelectDialogPriv* const d;
};


#endif // NAMESELECTDIALOG_H
