/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Martin Klapetek <email>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QPixmap>
#include <QPainter>
#include <QHash>
#include <QMultiHash>

#include <Nepomuk2/Vocabulary/PIMO>
#include <Nepomuk2/Vocabulary/NCO>
#include <Nepomuk2/ResourceManager>
#include <Nepomuk2/StoreResourcesJob>

#include <nepomuk2/simpleresource.h>
#include <nepomuk2/simpleresourcegraph.h>
#include <nepomuk2/datamanagement.h>

#include <Soprano/Model>
#include <Soprano/QueryResultIterator>
#include <Soprano/Vocabulary/NAO>

#include <KIconLoader>
#include <KMessageBox>
#include <KLocalizedString>
#include <KJob>
#include <KDebug>

#include "persons-model.h"
#include "person-object.h"
#include "pimo/tag.h"
#include <KCategorizedSortFilterProxyModel>

PersonsModel::PersonsModel()
{
    // Nepomuk2::ResourceManager::instance()->clearCache();

    QString query;
    query = "select distinct ?r ?l ?o where { ?r a pimo:Person ."
                                          "?r nao:prefLabel ?l ."
                                          "?r pimo:groundingOccurrence ?o ."
                                          "}";

    Soprano::QueryResultIterator it = Nepomuk2::ResourceManager::instance()->mainModel()->executeQuery(query,
                                                                                                      Soprano::Query::QueryLanguageSparql);

    QHash<QUrl, QString> nameHash;
    QMultiHash<QUrl, QUrl> occurenceHash;

    while(it.next()) {
        //kDebug() << Nepomuk2::Resource(it["r"].uri()).allProperties();
        nameHash.insert(it["r"].uri(), it["l"].literal().toString());
        occurenceHash.insertMulti(it["r"].uri(), it["o"].uri());
    }

    QHashIterator<QUrl, QString> hashIter(nameHash);
    while (hashIter.hasNext()) {
        hashIter.next();

        PersonObject *person = new PersonObject(hashIter.value(), this);
        person->addContacts(occurenceHash.values(hashIter.key()));

        m_data.append(qMakePair<PersonObject*, PersonType>(person, PersonsModel::ExistingPerson));
    }
    //kDebug() << nameHash << occurenceHash;
}

PersonsModel::~PersonsModel()
{
    //qDeleteAll(m_data);
}

QVariant PersonsModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    switch (role) {
        case Qt::DisplayRole:
            return m_data.at(index.row()).first->label();
        case Qt::DecorationRole:
            if (m_data.at(index.row()).second == PersonsModel::ExistingPerson) {
                return SmallIcon("user-identity", KIconLoader::SizeMedium);
            } else {
                QPixmap pixmap = KIconLoader::global()->loadIcon("user-identity", KIconLoader::NoGroup, 32);
                QPainter painter(&pixmap);
                KIcon("emblem-new").paint(&painter, 15, 0, 16, 16);

                return pixmap;
            }
        case KCategorizedSortFilterProxyModel::CategorySortRole:
            if (m_data.at(index.row()).second == PersonsModel::ExistingPerson) {
                return 2;
            } else {
                return 1;
            }
        case KCategorizedSortFilterProxyModel::CategoryDisplayRole:
            if (m_data.at(index.row()).second == PersonsModel::ExistingPerson) {
                return i18nc("Category headline in categorized view", "Persons in Nepomuk");
            } else {
                return i18nc("Category headline in categorized view", "Persons to be stored");
            }
        case PersonsModel::ItemRole:
            return QVariant::fromValue(m_data.at(index.row()).first);
        case PersonsModel::PersonTypeRole:
            return m_data.at(index.row()).second;
        default:
            return QVariant();
    }
}

int PersonsModel::rowCount(const QModelIndex& parent) const
{
    return m_data.size();
}

void PersonsModel::insertPerson(PersonObject* person)
{
    for (int i = 0; i < m_data.size(); i++) {
        if (m_data.at(i).first == person) {
            kDebug() << "Person already in model, emitting changed signal";
            m_data.replace(i, qMakePair<PersonObject*, PersonType>(person, PersonsModel::NotSavedPerson));
            emit dataChanged(createIndex(i, 0), createIndex(i, 0));
            return;
        }
    }

    beginInsertRows(QModelIndex(), m_data.size(), m_data.size());
    m_data.append(qMakePair<PersonObject*, PersonType>(person, PersonsModel::NotSavedPerson));
    endInsertRows();
}

void PersonsModel::savePersons()
{
    Nepomuk2::SimpleResourceGraph g;
    for (int i = 0; i < m_data.size(); i++) {
        PersonObject *person = m_data.at(i).first;
        Nepomuk2::SimpleResource personRes(person->uri());
        personRes.addType(Nepomuk2::Vocabulary::PIMO::Person());

        foreach(QUrl uri, person->allContacts()) {
            Nepomuk2::SimpleResource contactRes(uri);
            personRes.addProperty(Nepomuk2::Vocabulary::PIMO::groundingOccurrence(), contactRes);
        }

        personRes.addProperty(Soprano::Vocabulary::NAO::prefLabel(), person->label());

        g << personRes;
    }

    KJob *storeJob = Nepomuk2::storeResources(g);
    storeJob->exec();

    if (storeJob->error()) {
        KMessageBox::detailedSorry(0,
                                   i18nc("Error dialog text", "Saving the data into Nepomuk failed. Please report this."),
                                   storeJob->errorString());
    } else {
        kDebug() << "Data stored in Nepomuk.";
    }
}
