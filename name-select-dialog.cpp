/*
 *  A dialog which provides at least three choices, plus a cancel button
 *  Based on digiKam's NameSelectDialog class
 *
 *  Copyright (C) 2010-2011 by Marcel Wiesweg <marcel dot wiesweg at gmx dot de>
 *  Copyright (C) 2011  Martin Klapetek <martin dot klapetek at gmail dot com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "name-select-dialog.moc"

// Qt includes

#include <QSignalMapper>
#include <QToolBar>
#include <QToolButton>
#include <QLabel>
#include <QHBoxLayout>
#include <QGridLayout>
#include <KLocale>

// KDE includes

#include <kiconloader.h>
#include <kpushbutton.h>

class NameSelectDialog::NameSelectDialogPriv
{
public:

    NameSelectDialogPriv()
        : clicked(KDialog::None),
          iconSize(KIconLoader::SizeMedium),
          toolBar(0),
          secondSeparator(0)
    {
    }

    int               clicked;
    QSignalMapper     mapper;
    int               iconSize;
    QToolBar*         toolBar;
    QAction*          secondSeparator;

    void checkToolBar()
    {
        if (!toolBar)
        {
            toolBar = new QToolBar;
            toolBar->setOrientation(Qt::Vertical);
            toolBar->setIconSize(QSize(iconSize, iconSize));
            toolBar->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
            toolBar->addSeparator();
            secondSeparator = toolBar->addSeparator();
        }
    }
};

NameSelectDialog::NameSelectDialog(const QList<QString>& list, QWidget *parent)
    : KDialog(parent),
      d(new NameSelectDialogPriv)
{
    setButtons(Cancel);
    showButtonSeparator(false);

    //button(Ok)->setVisible(false);
    //button(Apply)->setVisible(false);

    connect(&d->mapper, SIGNAL(mapped(int)),
            this, SLOT(slotButtonClicked(int)));

    setPlainCaption(i18nc("@title:window", "Choose a label"));
    setShowCancelButton(true);

    QWidget* mainWidget = new QWidget;

    // -- Icon and Header --

    QLabel* warningIcon = new QLabel;
    warningIcon->setPixmap(SmallIcon("dialog-warning", KIconLoader::SizeHuge));
    QLabel* editIcon = new QLabel;
    editIcon->setPixmap(SmallIcon("document-edit", iconSize()));
    QLabel* question = new QLabel;
    question->setText(i18nc("@label", "There are several contacts with different names.<nl/>"
    "Which one do you want to use as a label for this person?"));

    QHBoxLayout* headerLayout = new QHBoxLayout;
    headerLayout->addWidget(question);
    headerLayout->addWidget(editIcon);

    // -- Central buttons --
    int key = 10000;

    foreach(QString label, list) {
        addChoiceButton(++key, "dialog-ok-apply", label);
    }

    // -- Layout --

    QGridLayout* mainLayout = new QGridLayout;
    mainLayout->addWidget(warningIcon, 0, 0, 2, 1, Qt::AlignTop);
    mainLayout->addLayout(headerLayout, 0, 1);
    //mainLayout->addLayout(buttonLayout);
    mainLayout->addWidget(buttonContainer(), 1, 1);
    //mainLayout->addWidget(new KSeparator(Qt::Horizontal));

    mainWidget->setLayout(mainLayout);
    setMainWidget(mainWidget);
}

NameSelectDialog::~NameSelectDialog()
{
    delete d;
}

void NameSelectDialog::setShowCancelButton(bool show)
{
    button(Cancel)->setVisible(show);
}

void NameSelectDialog::setIconSize(int size)
{
    d->iconSize = size;
}

int NameSelectDialog::iconSize() const
{
    return d->iconSize;
}

QToolButton* NameSelectDialog::addChoiceButton(int key, const QString& iconName, const QString& text)
{
    QToolButton* button = new QToolButton;
    button->setText(text);
    button->setIcon(SmallIcon(iconName, d->iconSize));
    button->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    button->setAutoRaise(true);
    button->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);

    d->mapper.setMapping(button, key);

    connect(button, SIGNAL(clicked()),
            &d->mapper, SLOT(map()));

    d->checkToolBar();
    d->toolBar->insertWidget(d->secondSeparator, button);

    return button;
}

QToolButton* NameSelectDialog::addChoiceButton(int key, const QString& text)
{
    return addChoiceButton(key, QString(), text);
}

QToolButton* NameSelectDialog::choiceButton(int key) const
{
    return qobject_cast<QToolButton*>(d->mapper.mapping(key));
}

int NameSelectDialog::clickedButton() const
{
    return d->clicked;
}

void NameSelectDialog::slotButtonClicked(int button)
{
    d->clicked = button;
    emit buttonClicked(static_cast<KDialog::ButtonCode>(button));

    if (button == Cancel)
    {
        reject();
    }
    else
    {
        accept();
    }
}

QWidget* NameSelectDialog::buttonContainer() const
{
    d->checkToolBar();
    return d->toolBar;
}
