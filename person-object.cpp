/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Martin Klapetek <email>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "person-object.h"

PersonObject::PersonObject(const QString& label, QObject *parent)
    : QObject(parent)
{
        m_label = label;
}

PersonObject::PersonObject(const PersonObject& other)
{
    setParent(other.parent());
    m_label = other.m_label;
    m_contacts = other.m_contacts;
}

PersonObject::~PersonObject()
{

}

void PersonObject::addContact(const QUrl& url)
{
    m_contacts << url;
}

void PersonObject::removeContact(const QUrl& url)
{
    m_contacts.removeAll(url);
}

QList<QUrl> PersonObject::allContacts()
{
    return m_contacts;
}

QString PersonObject::label()
{
    return m_label;
}

void PersonObject::setLabel(const QString& label)
{
    m_label = label;
}

QUrl PersonObject::uri()
{
    return m_uri;
}

void PersonObject::setUri(const QUrl& uri)
{
    m_uri = uri;
}

void PersonObject::addContacts(const QList<QUrl> contacts)
{
    m_contacts.append(contacts);
}
